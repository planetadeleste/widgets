### Trash Controller {#markdown-header-trash-controller}  
Add a controller  to manage trashed records (Model with `SoftDelete` trait).

#### Usage  
[See the official documentation](https://octobercms.com/docs/services/behaviors#usage-example)  
In your `Controller` add this line to `$implement`:
 
```php
    /**
     * Implement the TrashController behavior
     */
    public $implement = [
        'PlanetaDelEste.Widgets.Behaviors.TrashController'
    ];
```

Or if you want to extend a controller from your plugin, add this lines to `boot` method

```php
FooController::extend(function($controller) {

    // Implement behavior if not already implemented
    if (!$controller->isClassExtendedWith('PlanetaDelEste.Widgets.Behaviors.TrashController')) 		
    {
        $controller->implement[] = 'PlanetaDelEste.Widgets.Behaviors.TrashController';
    }

    // Add view path to override _list_toolbar.htm partial
    if(request()->segment(5) == 'trashed') {
        $controller->addViewPath('$/planetadeleste/widgets/behaviors/trashcontroller/partials');  
    }
}
```